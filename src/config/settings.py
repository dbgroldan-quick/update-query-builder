import os
from dotenv import load_dotenv
from pathlib import Path

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

_config = {
    'update_config': {
        'table_name': os.getenv("TABLE_NAME"),
        'field_name': os.getenv("FIELD_NAME"),
        'change_file_path': os.getenv("CHANGE_FILE_PATH")
    },
    'database_config': (os.getenv('DB_NAME'), os.getenv('DB_USER'),
        os.getenv('DB_HOST'), os.getenv('DB_PORT'),
        os.getenv('DB_PASSWORD')),
    'dml_path': os.getenv('DML_PATH')
}

def get_config():
    return _config