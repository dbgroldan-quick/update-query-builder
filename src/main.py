import sys
from database.sql_generator import updateQuery
from database.psql_connection import Connection
from utils.file_handler import generate_dml_file
from config.settings import get_config

config = get_config()

if __name__ == "__main__":
    info = "\t- INFORMATION:\n\tAdd Option:\n\t - sql \t\t Show SQL\n\t - dml \t\t Generate DML\n\t - migrate \t Migarte changes to database"
    if len(sys.argv) < 1:
        print(info)
    else:
        if sys.argv[1] == 'sql':
            print('\t ---------- Query -----------')
            print(updateQuery())
        elif sys.argv[1] == 'migrate':
            db_data = config['database_config']
            db_config = 'dbname=%s user=%s host=%s port=%s password=%s'%db_data
            connection = Connection(db_config)
            print('\t ... Migrating to Postgres ...')
            connection.executeQuery(updateQuery())
        elif sys.argv[1] == 'dml':
            print('\t ... Generating File ...')
            generate_dml_file(config['dml_path'], updateQuery())
        else:
            print(info)

