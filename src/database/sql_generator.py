from utils.file_handler import read_file
from config.settings import get_config

config = get_config()

def updateQuery():
    table_name = config['update_config']['table_name']
    field_name = config['update_config']['field_name']
    file_path = config['update_config']['change_file_path']
    all_data = read_file(file_path)
    query = ''
    for data in all_data:
        update_query = "UPDATE {0} SET {1} = '{2}' WHERE {1} = '{3}';".format(
            table_name,
            field_name, 
            data['new'],
            data['current']
        )
        query += '{}\n'.format(update_query)
    return query
