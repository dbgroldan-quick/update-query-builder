import json

def read_file(file_path):
    with open(file_path, 'r', encoding="utf-8") as file_data:
        data = json.load(file_data)
        return data['values']

def generate_dml_file(file_path, query):
        with open(file_path, 'w') as file_sql:
            file_sql.write(query)
            return {
                'message': 'Created DML file.',
                'path': file_path
            }