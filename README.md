# Update Query Builder
### Use
This is a basic service project with PostgreSQL, for to generate update DML.

## Designed by:
Daissi Bibiana Gonzalez Roldan
**E-Mail:** dbgroldan@gmail.com

### How to use this project
1. **Clone this Repo**
  ```sh
  → git clone
  ```
2. **Enter a option and ut the corrections in the change_file.json:**

Add Option:
- **sql**           Show SQL
- **dml**           Generate DML
- **migrate**       Migarte changes to database

3. **If there were no errors in the previous step**
  ```sh
  → docker build -t <container_name> .
  → docker run -it --rm -e API_TOKEN=<option> <container_name>
  ```


### Recommendations
*  If you want to install the project with a different configuration, remember to modify the environment variables in your **.env** file, following the example represented in  **.env.example**

* COuld to have problems with codecs in some SO (Windowss)


### License
MIT License