FROM python:3.8-alpine3.12
LABEL maintainer="dbgroldan"

WORKDIR /usr/src/app

RUN pip install --upgrade pip
COPY ./requirements.txt /usr/src/app/requirements.txt
RUN \
 apk add --no-cache postgresql-libs && \
 apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
 python3 -m pip install -r requirements.txt --no-cache-dir && \
 apk --purge del .build-deps

COPY . /usr/src/app/

ENV API_TOKEN=default_token

CMD python /usr/src/app/src/main.py ${API_TOKEN}